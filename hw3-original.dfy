//===============================================
// CS:5810 Formal Methods in Software Engineering
// Fall 2020
//
// Homework 3
//
// Name(s):  
//
//===============================================

//===========
// Problem 1
//===========

// method arrayMax(a: array<nat>) returns (m: int)
//   requires ; // (precondition)
//   ensures ; // (postcondition)
// {
// }

//===========
// Problem 2
//===========

function fcount(n:int, a: array<int>, k:int): int
{
  if k == a.Length then 
    0
  else if n == a[k] then
    fcount(n, a, k + 1) + 1
  else
    fcount(n, a, k + 1)
}

method count(n:int, a: array<int>) returns (c: int)
{
  var i:nat := 0;
  c := 0;
  while (i < a.Length)
  {
    if (n == a[i]) {c := c + 1;}
    i := i + 1;
  }
}


//===========
// Problem 3
//===========

method closestValue(a: array<int>, k: int) returns (p: nat)
{

}



//===========
// Problem 4
//===========

function maxUpTo(a: array<int>, n: nat): int
{

}

function minUpTo(a: array<int>, n: nat): int
{

}

method range(a: array<int>) returns (r: int)
{
  var min := a[0];
  var max := a[0];
  var i:nat := 1;

  while (i < a.Length)
  {
    if (min > a[i]) { min := a[i]; }
    if (max < a[i]) { max := a[i]; }
    i := i + 1;
  }
  r := max - min;
}

//===========
// Problem 5
//===========

method isPrefix(a: array<int>, b: array<int>) returns (r: bool)
{
  var i := 0 ;
  r := true ;
  while (i < a.Length)
  {
    r := r && (a[i] == b[i]);
    i := i + 1;
  }
}

