//===============================================
// CS:5810 Formal Methods in Software Engineering
// Fall 2020
//
// Homework 3
//
// Name(s):  Alex Hubers, Jamil Gafur
//
//===============================================

//===========
// Aux functions
// 
// These are helpful for both Problem 1 and 4, so they're 
// written up here.
//===========

function max(a: int, b: int): int
{
  if a > b then a else b
}
function min(a: int, b: int): int
{
  if a < b then a else b
}

// foldUpTo is a generalization of a folding "comp" 
// over `a` from position to n - 1 to 0.
function foldUpTo<T>(a: array<T>, n: nat, comp: (T, T) -> T): T
reads a
requires a.Length > 0
requires 0 < n <= a.Length
decreases n
{
  if n == 1 then a[n - 1] else comp(
    foldUpTo(a, n - 1, comp), 
    a[n - 1]
  )
}

// if you concretize n to be a.Length,
// you just get rfold.
function rfold<T>(a: array<T>, comp: (T, T) -> T): T
reads a
requires a.Length > 0
{
  foldUpTo(a, a.Length, comp)
}

function maxUpTo(a: array<int>, n: nat): int
reads a
requires a.Length > 0
requires 0 < n <= a.Length
decreases n
{
  foldUpTo(a, n, max)
}

function minUpTo(a: array<int>, n: nat): int
reads a
requires a.Length > 0
requires 0 < n <= a.Length
decreases n
{
  foldUpTo(a, n, min)
}


//===========
// Problem 1
//===========

method arrayMax(a: array<int>) returns (m: int)
  // Preconditions:
  // a should be nonempty
  requires a.Length > 0;

  // post conditions:
  // m is greater the greatest element of a.
  ensures m == rfold(a, max);
{

  var i := 1;
  m := a[i - 1];

  while (i < a.Length)
    // i never exceeds a.length
    invariant 1 <= i <= a.Length
    // m is the maximum of the subsection of what we've yet iterated over.
    invariant m == maxUpTo(a, i)
    decreases a.Length - i; 
  {
    if a[i] > m
    {
      m := a[i];
    }
    i := i + 1;
  }
}

//===========
// Problem 2
//===========
 
// fcount() takes an additional input k between 0 and a.Length (both included), 
// and returns the number of times n occurs in the slice of a 
// from position k (included) to the end of the array.
function fcount(n:int, a: array<int>, k:int): int
reads a;
// Pre conditions:
// k is within the correct bounds.
requires 0 <= k <= a.Length;

// Termation guarantee --
// k grows by 1 at each step until reaching a.Length.
decreases a.Length - k;
{
  if k == a.Length then 
    0
  else if n == a[k] then
    fcount(n, a, k + 1) + 1
  else
    fcount(n, a, k + 1)
}

// 
// count() takes integer n and an integer array a and
// returns number of times n occurs in a.
method count(n:int, a: array<int>) returns (c: int)
// pre-conditions:
// none necessary

// Post-conditions:
// count(n, a) should equal fcount(n, a, k) when k = 0.
ensures c == fcount(n, a, 0)
{
  var i: nat := 0;
  c := 0;

  while (i < a.Length)
  invariant 0 <= i <= a.Length
  invariant fcount(n, a, 0) == c + fcount(n, a, i)
  decreases a.Length - i  
  {
    if (n == a[i]) {c := c + 1;}
    i := i + 1;
  }
}

//===========
// Problem 3
//===========

function method abs(x:int): nat
ensures if x >= 0 then abs(x) == x else abs(x) == -x
{
  if x < 0 then -x else x
}

// 	given an array a of integers and integer value k,
// 	returns the index p of the value in a that is closest to k.
method closestValue(a: array<int>, k: int) returns (p: nat)
// Pre-Conditions:
// we're going to make an assumption that a is nonempty,
// otherwise no such p exists, as p is by definition an index of a, and
// a would have no indices.
requires a.Length > 0;

// Post-Conditions
// p is indeed an index of a.
ensures 0 <= p < a.Length;
// a[p] is "closer" to k than any arbitrary i in a.
ensures forall i: int :: 0 < i < a.Length ==> abs(a[p] - k) <= abs(a[i] - k);

{
  var i := 0;
  
  // We initialize assuming the first
  // element is the closest value we've yet seen.
  var diff: int := abs(a[i] - k);
  p := 0;

  // iterate through array
  while(i < a.Length)
    invariant 0 <= i <= a.Length
    invariant 0 <= p < a.Length
    invariant diff == abs(a[p] - k)
    invariant forall j : int :: 0 <= j < i ==> diff <= abs(a[j] - k)
    decreases a.Length - i
  {
    var newDiff := abs(a[i] - k);
    if  newDiff < diff
    {
      // update diff and save position
      diff := newDiff;
      p := i;
    }

    i := i + 1;
  }
}

//===========
// Problem 4
//===========

method range(a: array<int>) returns (r: int)
requires a.Length > 0
ensures r == rfold(a, max) - rfold(a, min)
{
  var min := a[0];
  var max := a[0];
  var i: nat := 1;

  while (i < a.Length)
  invariant 1 <= i <= a.Length
  invariant max == maxUpTo(a, i)
  invariant min == minUpTo(a, i)
  decreases a.Length - i
  {
    if (min > a[i]) { min := a[i]; }
    if (max < a[i]) { max := a[i]; }
    i := i + 1;
  }
  r := max - min;
}

//===========
// Problem 5
//===========

function isPrefixUpTo(a: array<int>, b: array<int>, n: nat): bool
reads a
reads b
requires 0 <= n <= a.Length
requires b.Length >= a.Length
decreases n
{
  // if a.Length or n == 0 we arbitrarily return true to match
  // the empty-case behavior of isPrefix.
  if a.Length == 0 || n == 0 then true
  else if n == 1
      then a[n - 1] == b[n - 1]
      else isPrefixUpTo(a, b, n - 1) && a[n - 1] == b[n - 1]
}

method isPrefix(a: array<int>, b: array<int>) returns (r: bool)
requires b.Length >= a.Length

// all of `a` should be a prefix of `b`.
ensures r == isPrefixUpTo(a, b, a.Length)
{
  var i := 0 ;
  r := true ;
  while (i < a.Length)
  invariant 0 <= i <= a.Length
  invariant r == isPrefixUpTo(a, b, i)
  decreases a.Length - i
  {
    r := r && (a[i] == b[i]);
    i := i + 1;
  }
}

//===========
// Problem 5 (Extra Credit)
// Note: Problem not correct, but I'm curious if this was the right track.
//===========

function isPrefixUpTo2(a: array<int>, b: array<int>, i: nat, n: nat, prev: bool): bool
reads a
reads b
requires 0 <= i <= n
requires 0 <= n <= a.Length
requires b.Length >= a.Length
decreases n - i
{
  // return the prev value if
  // - the previous value was false
  // - i has reached n
  // - the list is empty
  // - n is 0
  if !prev || i == n || a.Length == 0 || n == 0 then prev
  else isPrefixUpTo2(a, b, i + 1, n, a[i] == b[i])
}

method isPrefix2(a: array<int>, b: array<int>) returns (r: bool)
requires b.Length >= a.Length

ensures r == isPrefixUpTo2(a, b, 0, a.Length, true)
{
  var i := 0;
  r := true;
  var prev := r;
  while (i < a.Length && r)
  invariant 0 <= i <= a.Length
  invariant r == isPrefixUpTo2(a, b, 0, i, r)
  decreases a.Length - i
  {
    prev := r;
    r := a[i] == b[i]; 
    i := i + 1;
  }
}


