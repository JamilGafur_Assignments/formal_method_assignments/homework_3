function Fib(n: nat): nat 
decreases n
{
    if n < 2 then n else Fib(n - 2) + Fib(n - 1)
}

method ComputeFib(n: nat) returns (x: nat)
ensures x == Fib(n)
{
    x := 0;
    var i, y := 0, 0;
    while i != n
    invariant 0 <= i <= n
    invariant x == Fib(i)
    decreases n - i
    {
        x, y := y, x + y;
        i := i + 1;
    }
}